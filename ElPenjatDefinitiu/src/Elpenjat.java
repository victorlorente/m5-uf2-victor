
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Elpenjat {
	/**
	 * @version 1-2021
	 * @author victor
	 * @since 28-1-2021
	 * @param args
	 */

	public static void main(String[] args) {
		boolean trafficlight = false;

		// Numero de lletres abans de perdre, que li pasarem a Game()
		int difficulty = 10;

		// VARIABLES DE DADES
		ArrayList<String> players = new ArrayList<String>();
		ArrayList<Integer> intentos = new ArrayList<Integer>();
		ArrayList<Integer> partidasGanadas = new ArrayList<Integer>();

		ArrayList<String> palabras = new ArrayList<String>();
		palabras.add("ALOLA");
		palabras.add("AXEL");
		palabras.add("CHARMANDER");
		palabras.add("PEDROSA");
		palabras.add("MARIPOSA");

		do {
			printMenu();
			switch (scannerMenu(4)) {
			case 1:
				printHelp();
				break;
			case 2:
				// Crea un nou jugador en l'array i li genera els intents i les partides
				// guanyades que tendran el mateix index
				players = setPlayer(players);
				intentos.add(0);
				partidasGanadas.add(0);
				break;
			case 3:

				// Escolleix jugador
				String jugadorAhora;
				int jugadorPosicion = -1;

				if (!players.isEmpty()) {
					boolean ok;
					do {
						ok = false;
						// L'usuari escolleix un nom de un jugador ja exitent
						jugadorAhora = scannerPlayerName();
						for (int i = 0; i < players.size(); i++) {
							if (players.get(i).equals(jugadorAhora)) {
								ok = true;
								jugadorPosicion = i;
							}
						}
						if (!ok) {
							printPlayerNoExistError();
						}
					} while (!ok);

					// Suma 1 a intents
					intentos.set(jugadorPosicion, intentos.get(jugadorPosicion) + 1);

					Random rand = new Random();

					if (game(palabras.get(rand.nextInt(palabras.size())), difficulty)) {
						// Suma 1 a partidasGanadas si guanya
						partidasGanadas.set(jugadorPosicion, partidasGanadas.get(jugadorPosicion) + 1);
						// Et felicita per haver guanyat
						printWinMessage();
					} else {
						// Et diu que has perdut
						printLoseMessage();
					}
				} else {
					printNoPlayerError();
				}
				break;
			case 4:
				if (!players.isEmpty()) {
					printAllPlayerData(players, intentos, partidasGanadas);
				} else {
					printNoPlayerError();
				}
				break;
			case 0:
				System.out.println("Sortint...");
				System.exit(0);
				break;
			default:
				System.out.println("Default");
				break;
			}
		} while (!trafficlight);
	}

	// Scanners

	/**
	 * Demana escollir una opcio del menu
	 * 
	 * @param NumberOptions
	 * @return
	 */
	public static int scannerMenu(int NumberOptions) {
		// La inicialitzo perque sino el return es queixa
		int option = -1;
		Scanner reader = new Scanner(System.in);

		do {
			// Si dona error es que no es un Int
			// Demana que introdueixis un numero
			printAskMenu();
			try {
				option = reader.nextInt();
			} catch (Exception e) {

				System.out.println("Tenen que ser enters!");
				reader.nextLine();
			}
			// Sortira del bucle si es un numero valid, gracies a especificar-ho a
			// NumberOptions
		} while (option < 0 || option > NumberOptions);
		return option;
	}

	/**
	 * 
	 * @return
	 */
	public static String scannerPlayerName() {
		Scanner reader = new Scanner(System.in);
		printAskName();
		String players = reader.nextLine();
		return players;
	}

	/**
	 * Comprova que sigui un caracter
	 * 
	 * @param alreadyTested
	 * 
	 */
	public static char scannerAnsweredChar(ArrayList<Character> alreadyTested) {
		Scanner reader = new Scanner(System.in);
		char character;
		boolean ok;
		do {
			printAskChar();
			character = reader.next().charAt(0);
			// Converteix la lletra a majuscula
			character = Character.toUpperCase(character);
			ok = true;

			if (Character.isLetter(character)) {
				for (char c : alreadyTested) {
					if (character == c) {
						ok = false;
						printCharacterAlreadyAnswered();
					}
				}
			} else {
				printNoLetterError();
				ok = false;
			}
		} while (!ok);

		// Converteix la lletra a majuscula
		character = Character.toUpperCase(character);
		return character;
	}

	/**
	 * Prints
	 * 
	 */
	public static void printMenu() {
		System.out.print("\nEl joc del Penjat: \n");
		System.out.print("1. Mostrar Ajuda");
		System.out.print("\n2. Definir Jugador");
		System.out.print("\n3. Jugar Partida");
		System.out.print("\n4. Veure Jugador");
		System.out.print("\n0. Sortir");
		System.out.println("\n----------------------------------------------");
	}

	public static void printAskMenu() {
		System.out.print("\nIntrodueix una opci� :");
	}

	public static void printHelp() {
		System.out.println("");
		System.out.println("****  Instrucciones juego del Ahorcado  ******");
		System.out.println("----------------------------------------------");
		System.out.println(
				"El juego consiste en adivinar una palabra secreta seleccionada por el programa de forma aleatoria.");
		System.out.println(
				"Al comienzo del juego, el programa muestra una sucesi�n de guiones. Cada gui�n contiene una letra de la palabra que se ha de adivinar.");
		System.out.println(
				"En cada tirada el jugador indica una letra. Si la letra est� dentro de la palabra, se muestra, tantas veces como aparezca, sustituyendo el gui�n correspondiente por la posici�n que ocupa.");
		System.out.println(
				"Si la letra indicada por el jugador no est� en la palabra, el jugador tendr� un fallo. Se pierde la partida, si se falla m�s de 10 veces; y se gana si se adivina completamente la palabra sin superar los 10 fallos.");
		System.out.println("Mucha suerte y a jugar....");
	}

	public static void printAskName() {
		System.out.println("Nom del jugador: \n");
	}

	public static void printAskChar() {
		System.out.println("Escriu una lletra: ");
	}

	public static void printSamePlayer() {
		System.out.println("El jugador ja existeix! \n");
	}

	public static void printNoPlayerError() {
		System.out.println("No hi ha cap jugador registrat!");
	}

	public static void printNoNumberError() {
		System.out.println("T� que ser un enter!");
	}

	public static void printNoLetterError() {
		System.out.println("Te que ser una lletra!");
	}

	public static void printNumberOutOfBoundsError(int NumberOptions) {
		System.out.println("Tenen que ser un numero entre el 0 al " + NumberOptions + "!");
	}

	public static void printCorrectWord(char a) {
		System.out.println("La lletra " + a + " es correcta!");
	}

	public static void printWordsTested(ArrayList<Character> chars) {
		System.out.print("Lletras probades: ");
		for (char c : chars) {
			System.out.print(c + " ");
		}
		System.out.println(".");
	}

	public static void printPlayerNoExistError() {
		System.out.println("El jugador no exiteix!");
	}

	public static void printCurrentWord(char[] blockword) {
		for (int i = 0; i < blockword.length; i++) {
			System.out.print(blockword[i] + " ");
		}
	}

	public static void printCharacterAlreadyAnswered() {
		System.out.println("Ja has intentat aquesta lletra!");
	}

	public static void printWinMessage() {
		System.out.println("Has guanyat!");
	}

	public static void printLoseMessage() {
		System.out.println("Has perdut!");
	}

	public static void printAllPlayerData(ArrayList<String> players, ArrayList<Integer> intentos,
			ArrayList<Integer> partidasGanadas) {
		System.out.println("---------------------------------------");
		for (int i = 0; i < players.size(); i++) {
			System.out.println("Nom del jugador: " + players.get(i));
			System.out.println("Intents: " + intentos.get(i));
			System.out.println("Partides guanyades: " + partidasGanadas.get(i));
			System.out.println("---------------------------------------");
		}
	}

	/**
	 * @param players Demana i guarda un jugador nou, prevenint que es repeteixin
	 *                els noms dels jugadors
	 */

	public static ArrayList<String> setPlayer(ArrayList<String> players) {
		boolean ok = false;
		String newPlayerName;
		do {
			newPlayerName = scannerPlayerName();
			ok = true;
			for (String arrayPlayerName : players) {
				if (newPlayerName.equals(arrayPlayerName)) {
					ok = false;
					printSamePlayer();
				}
			}
		} while (!ok);
		players.add(newPlayerName);
		return players;
	}

	// Aqui s'executa el joc
	public static boolean game(String word, int difficulty) {

		// Numero de lletres correctes
		int correct = 0;

		// Guanya o no guanya
		boolean win = false;

		// Intents
		int intentos = 0;

		// Lletres que han sigut probades
		ArrayList<Character> charTested = new ArrayList<Character>();

		// Converteixo la paraula en un array de chars i la cambio cada lletra a _
		char palabraRespondida[] = word.toCharArray();
		for (int i = 0; i < palabraRespondida.length; i++) {
			palabraRespondida[i] = '_';

		}

		boolean ok = true;

		// Aqui dintre passa tot el joc
		do {
			// Mostra paraula a adivinar
			printCurrentWord(palabraRespondida);

			// Pregunta una lletra, comprobant que no posis ninguna que ja hagis posat
			char charAnswered = scannerAnsweredChar(charTested);

			// Afegeix aquesta lletra a l'array de lletres probades, sigui correcta o no
			charTested.add(charAnswered);

			// Suma un intent
			intentos++;

			boolean felicitado = false;

			// Compara totes les lletres de la paraula a adivinar
			for (int i = 0; i < word.length(); i++) {
				// Si es correcte entra aqui
				if (word.charAt(i) == charAnswered) {
					// Afegeix 1 punt a correct
					correct++;

					// Cambia el simbol _ per la lletra correcta de les posicions amb la mateixa
					// lletra que la lletra adivinada
					palabraRespondida[i] = charAnswered;

					if (!felicitado) {
						printCorrectWord(charAnswered);
						felicitado = true;
					}
				}
			}
			// Imprimeix les lletres que ha has usat
			printWordsTested(charTested);

			// Si el numero de lletres correctes es igual a la longitud de la paraula, vol
			// dir que les has adivinar totes i has guanyat el joc :)
			if (correct == palabraRespondida.length) {
				ok = false;
				win = true;
			}

			// Si el numero d'intents es igual al de la dificultat (numero d'intents
			// permesos), vol dir que has perdut el joc :(
			if (intentos == difficulty) {
				ok = false;
				win = false;
			}

		} while (ok);

		return win;
	}

}
