import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class irpfTest {

	// Resultat correcte
	@Test
	public void testSalari1000() {
		double res = IRPF.irpf(1000);
		assertEquals(9720, res);
		// fail("Not yet implemented");
	}

	@Test
	public void testSalari1300() {
		double res = IRPF.irpf(1300);
		assertEquals(11856, res);
		// fail("Not yet implemented");
	}

	@Test
	public void testSalari1900() {
		double res = IRPF.irpf(1900);
		assertEquals(15960, res);
		// fail("Not yet implemented");
	}

	@Test
	public void testSalari3100() {
		double res = IRPF.irpf(3100);
		assertEquals(23436, res);
		// fail("Not yet implemented");
	}

	@Test
	public void testSalari6100() {
		double res = IRPF.irpf(6100);
		assertEquals(40260, res);
		// fail("Not yet implemented");
	}

	public void testSalari1000A() {
		double res = IRPF.irpfAndorra(1000);
		assertEquals(12000, res);
		// fail("Not yet implemented");
	}

	@Test

	public void testSalari1300A() {
		double res = IRPF.irpfAndorra(1300);
		assertEquals(15600, res);
		// fail("Not yet implemented");
	}

	@Test

	public void testSalari1900A() {
		double res = IRPF.irpfAndorra(1900);
		assertEquals(22800, res);
		// fail("Not yet implemented");
	}

	@Test

	public void testSalari3100A() {
		double res = IRPF.irpfAndorra(3100);
		assertEquals(35340, res);
		// fail("Not yet implemented");
	}

	@Test

	public void testSalari6100A() {
		double res = IRPF.irpfAndorra(6100);
		assertEquals(65880, res);
		// fail("Not yet implemented");
	}

	// Test de resultat incorrecte (fallida)
	@Test

	public void testSalariFallida() {
		double res = IRPF.irpf(1111111);
		assertEquals(111111, res);
		// fail("Not yet implemented");
	}

	@Test
	public void testSalariFallida1() {
		double res = IRPF.irpfAndorra(1111111);
		assertEquals(100000, res);
		// fail("Not yet implemented");
	}

	@Test
	public void testSalariFallida2() {
		double res = IRPF.irpf(2000.2345);
		assertEquals(1000.923, res);
		// fail("Not yet implemented");
	}

}
