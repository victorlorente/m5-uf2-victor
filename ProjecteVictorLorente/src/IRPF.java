import java.util.Scanner;
import java.util.ArrayList;
/**
 * <h2> Projecte IRPF </h2>
 * 
 * @author Victor Lorente
 * @version 2.0
 * 
 */
public class IRPF {
	/**
	 * 
	 * @param args  �s el main del programa on s'executar� tot el proc�s.
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		double salari = 0;
		int casos = 0;
		casos = reader.nextInt();
		for (int i = 0; i < casos; i++) {
			salari = reader.nextInt();
			double espanya = irpf(salari);
			double andorra = irpfAndorra(salari);
			System.out.println(irpf(salari) + " " + irpfAndorra(salari));
			missatgeSalari(espanya, andorra);
		}
	}

	/**
	 * 
	 * @param salari Rep el salari mensual que despr�s es multiplicar� per 12 que
	 *               ser� el anual
	 * @return Et retorna el salari net anual en Espanya depenen dels ingressos
	 *         mensuals que reps
	 */
	public static Double irpf(double salari) {
		double salariany = salari * 12;
		double impost = 0;
		double salarinet = 0;
		if (salariany < 12450) {
			impost = salariany * 0.19;
			salarinet = salariany - impost;
		}
		if (salariany >= 12450 && salariany < 20200) {
			impost = salariany * 0.24;
			salarinet = salariany - impost;
		}
		if (salariany >= 20200 && salariany < 35200) {
			impost = salariany * 0.30;
			salarinet = salariany - impost;
		}
		if (salariany >= 35200 && salariany < 60000) {
			impost = salariany * 0.37;
			salarinet = salariany - impost;
		}
		if (salariany >= 60000) {
			impost = salariany * 0.45;
			salarinet = salariany - impost;
		}
		return salarinet;
	}

	/**
	 * 
	 * @param salari Rep el salari mensual que despr�s es multiplicar� per 12 que
	 *               ser� el anual
	 * @return Et retorna el salari net anual en Andorra.
	 */
	public static Double irpfAndorra(double salari) {
		double salariany = salari * 12;
		double impost = 0;
		double salarinet = 0;
		if (salariany < 24000) {
			salarinet = salariany;
		}
		if (salariany >= 24000 && salariany < 40000) {
			impost = salariany * 0.05;
			salarinet = salariany - impost;
		}
		if (salariany >= 40000) {
			impost = salariany * 0.10;
			salarinet = salariany - impost;
		}
		return salarinet;
	}

	/**
	 * 
	 * @param esp Rep per par�metre el salari net d'Espanya
	 * @param and Rep per par�metre el salari net d'Andorra Depenen de la diferencia
	 *            de diners mostrar� un missatge o un altre El valor dependr� si la
	 *            diferencia �s m�s de 5000 euros
	 */
	public static void missatgeSalari(double esp, double and) {
		double salari = 0;
		salari = and - esp;
		if (salari >= 5000) {
			System.out.println("Ens anem a Andorra!");
		} else {
			System.out.println("Encara no soc Elrubius!");
		}
	}

}
